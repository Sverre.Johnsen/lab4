package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState cellstate;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        this.cellstate = initialState;


	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub


        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub


        if ((numRows() < row) || (row < 0)) {
            throw new IndexOutOfBoundsException("jaja");

        }
        if ((numColumns() < column) || (column < 0)) {
            throw new IndexOutOfBoundsException("neienei");
        }
        grid[row][column] = element;


    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        CellState pos = grid[row][column];
        return pos;
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid newGrid = new CellGrid(rows,columns,cellstate);

        //CellState[][] copy = new CellState[][];
        /***/

        for(int i=0; i<rows ; i++)
            for ( int j=0; j < columns; j++)
                newGrid.set(i,j,get(i,j));

        return newGrid;
    }
    
}
