package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;
    private int rows;
    private int columns;


    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
        this.rows = rows;
        this.columns = columns;
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }

    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row=0; row<numberOfRows();row++) {
            for (int col=0; col<numberOfColumns();col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        currentGeneration=nextGeneration;

    }


    @Override
    public int numberOfRows() {
        return rows;
    }

    @Override
    public int numberOfColumns() {
        return columns;
    }


    @Override
    public CellState getNextCell(int row, int col) {

        CellState cellState = getCellState(row, col);
        int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);

        if (cellState==CellState.ALIVE) {
            return CellState.DYING;
        }
        else if (cellState==CellState.DYING){
            return CellState.DEAD;
        }
        else if (cellState==CellState.DEAD && aliveNeighbors==2 ) {
            return CellState.ALIVE;
        }
        else {
            return CellState.DEAD;
        }
    }

    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int col, CellState state) {
        int stateCount = 0;
        int nRow=numberOfRows();
        int nCol=numberOfColumns();



        for (int height=-1; height<=1; height++) {
            for (int width=-1; width<=1; width++) {
                if (height==0 && width ==0) {
                    continue;
                }
                if (height+row>=nRow || height+row<0 || width+col>=nCol || width + col<0) {
                    continue;
                }
                if (getCellState(row+height, col+width) == state) {
                    stateCount++;
                }
            }
        }
        return stateCount;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
